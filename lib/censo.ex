defmodule Censo do
  def main do
    []
    |> main_menu
  end

  def main_menu(payroll) do
    IO.puts("Menu principal")

    option =
      IO.gets("1. Digitador.\n2. Consultor\n0. Salir\n")
      |> String.trim()

    case option do
      "1" ->
        payroll |> insert_employee

      "2" ->
        payroll |> reporting_menu

      "0" ->
        System.halt(0)

      _ ->
        payroll |> main_menu
    end
  end

  def reporting_menu(payroll) do
    IO.puts("Menu de consulta")

    option =
      IO.gets("""
      1. Listar empleados
      2. Listar nombres
      3. Listar edades
      4. Listar sexos
      5. Listar profesiones
      6. Listar sueldos
      7. Presupuesto total
      0. Regresar
      """)
      |> String.trim()

    case option do
      "1" -> payroll |> list_employees |> reporting_menu

      "2" -> payroll |> list_names |> reporting_menu

      "3" -> payroll |> list_ages |> reporting_menu

      "4" -> payroll |> list_sex |> reporting_menu

      "5" -> payroll |> list_professions |> reporting_menu

      "6" -> payroll |> list_salaries |> reporting_menu

      "7" -> payroll |> total_budget |> reporting_menu

      "0" -> payroll |> main_menu

      _ -> payroll |> reporting_menu
      payroll
    end
  end

  def total_budget(payroll) do
    payroll
  end

  def list_employees(payroll) do
    IO.puts("Empleado\tEdad\tSexo\tProfesion\tSalario")
    for employee <- payroll do
      IO.puts("#{employee.name}\t#{employee.age}\t#{employee.sex}\t#{employee.profession}\t#{employee.salary}")
    end
    payroll
  end

  def list_names(payroll) do
    Enum.map payroll, fn(employee) ->
      employee.name
    end
  end

  def list_ages(payroll) do
    Enum.map payroll, fn(employee) ->
      employee.age
    end
  end

  def list_sex(payroll) do
    Enum.map payroll, fn(employee) ->
      employee.sex
    end
  end

  def list_professions(payroll) do
    Enum.map payroll, fn(employee) ->
      employee.profession
    end
    payroll
  end

  def list_salaries(payroll) do
    Enum.map payroll, fn(employee) ->
      employee.salary
    end
    payroll
  end

  def insert_employee(payroll) do
    IO.puts("Ingrese los datos del empleado")
    name = IO.gets("Nombre: ") |> String.trim()
    {age, _} = IO.gets("Edad: ") |> String.trim() |> Integer.parse()
    sex = IO.gets("Sexo: ") |> String.trim()
    profession = IO.gets("Profesion: ") |> String.trim()
    {salary, _} = IO.gets("Sueldo: ") |> String.trim() |> Float.parse()

    payroll ++ [%Censo.Employee{
      name: name,
      age: age,
      sex: sex,
      profession: profession,
      salary: salary
    }]
    |> main_menu
  end
end
