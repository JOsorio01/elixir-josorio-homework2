defmodule Censo.Employee do
  defstruct name: nil, age: nil, sex: nil, profession: nil, salary: nil
end
